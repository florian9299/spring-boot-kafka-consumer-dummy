package de.appinstalls.springbootkafkaconsumerdummy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootKafkaConsumerDummyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootKafkaConsumerDummyApplication.class, args);
	}

}
