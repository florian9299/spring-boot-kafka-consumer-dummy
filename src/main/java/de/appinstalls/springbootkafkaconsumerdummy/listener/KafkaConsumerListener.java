package de.appinstalls.springbootkafkaconsumerdummy.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumerListener {

    @KafkaListener(topics = "OV-MA-CS-OM-DA-AppInstalls-tok", groupId = "twainAppInstallKafkaDummy")
    public void consume(String message) {
        System.out.println(
                "Consumed message: " + message
        );
    }
}
